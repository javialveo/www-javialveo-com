"use strict";

function viewMenu() {
  const containerMenu = document.querySelector("#menuWeb");
  
  containerMenu.classList.toggle("menu--hide");
}

function main() {
  const buttonMenu = document.querySelector("#buttonMenu");
  
  buttonMenu.addEventListener("click", viewMenu);
}

main();
